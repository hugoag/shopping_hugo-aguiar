import './index.css';
import 'jquery';
import 'bootstrap/dist/js/bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
window.$ = window.jQuery = require("jquery")

import {getProducts} from './api/userApi';

getProducts().then(result =>{
    let productsBody = "";

    result.forEach(product => {
        var img = require('./img/' + product.image );
        productsBody +=`<li>
        
        <p><img src="${img}" class="img-responsive" alt="Responsive Image"/></p>
        <p class="productTitle">${product.title}</p>
        <p class="productDescription">${product.description}</p>
        <p class="productPrice">${product.price}</p>
        <p><a type="button" class="btn btn-default addToBag">Add to Bag</a></p>
        </li>
        `
    });
    global.document.getElementById("productList").innerHTML = productsBody;

    
});


 window.$(document).ready(function() {
        window.$('ul').on( "click", "a.addToBag", function(e) {
            var SendButton = window.$(e.target);
            var productHtml =  window.$(SendButton).closest('li').html();
           addToBag(productHtml)
        });

        window.$('ul').on( "click", "a.removeBag", function(e) {
            var SendButton = window.$(e.target);
            window.$(SendButton).closest('li').remove();
        });
    });

function addToBag(productHtml){
     window.$(productHtml).find('a.addToBag').removeClass('addToBag').addClass('removeBag');
     var e = document.createElement('li');
     e.innerHTML = productHtml;
     global.document.getElementById("myBag").appendChild(e);
     window.$('#myBag').find('a.addToBag').text('Remove from Bag').removeClass('addToBag').addClass('removeBag');
    
}

