import webpack from 'webpack';
import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';

export default {
    debug : true,
    devtool: 'inline-source-map',
    noInfo : false,
    entry : [
        path.resolve(__dirname, 'src/index'),
    ],
    target : 'web',
    output: {
        path : path.resolve(__dirname, 'src'),
        publicPath: '/',
        filename: 'bundle.js'
    },
    plugins : [
        // Create HTML file that includes reference to bundled js
        new HtmlWebpackPlugin({
            template: 'src/index.html',
            inject : true
        }),
        new webpack.ProvidePlugin({   
            jQuery: 'jquery',
            $: 'jquery',
            jquery: 'jquery'
        })
    ],
    module: {
        loaders : [
            {test : /\.js$/, exclude: /node_modules/, loaders: ['babel']},
            {test : /\.css$/, loaders : ['style', 'css']},
            {test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/, loader: 'file-loader'},
            {test: /\.(png|jpg)$/, exclude: /node_modules/, loader: 'url-loader?limit=100000' }
        ]
    }
}