
import express from 'express';
import path from 'path';
import open from 'open';
import compression from 'compression';

/* eslint-disable no-console */

const port = 3000;
const app = express();

app.use(compression());
app.use(express.static('dist'));

app.get('/', function(req, res){
    res.sendFile(path.join(__dirname, '../dist/index.html'));
});

app.get('/products', function(req, res){
    // Hard coding for simplicity. Pretend this hits a real database

    res.json([
        { "id": 1, "title" : "Bob", "description" : "Smith", "price" : 700, "discount" : 40},
        {"id": 2, "title" : "Tammy", "description" : "Norton", "price" : 680, "discount" : 50},
        {"id": 3, "title" : "Tina", "description" : "Lee", "price" : 750, "discount" : 40}
    ]);
});


app.listen(port, function(err){
    if(err){
        console.log(err);
    }else {
        open('http://localhost:' + port);
    }
});